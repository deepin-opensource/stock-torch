# stockTorch

#### 介绍
学习量化时候写的一些代码。

#### 软件架构
python+pytorch+seaborn+pandas+akshare


#### 安装教程

1.  安装虚拟环境：pipenv install
2.  激活虚拟环境：pipenv shell

#### 使用说明

1.  `01-torch`里面是深度学习的代码，`02-statistics`里面是一些量化分析的代码，老铁们自取即可。
2.  使用`01-torch`模块分析其他股票和周期，只需要在`data = MyData()`中传入`name`、`code`和`days4train`参数即可，`name`是股票的名字，`days4train`是你要分析的周期，比如30日线就填30，60日线就填60。
#### 最近预测
<center>

![](01-torch/todayPre.png)
</center>

#### 涨跌幅实际验证曲线
<center>

![](01-torch/check.png)
</center>

#### 正确率分布
<center>

![](01-torch/ratio.png)

</center>

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
