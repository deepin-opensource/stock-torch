from peewee import (
    CharField,
    DateField,
    FloatField,
    IntegerField,
    Model,
    SqliteDatabase,
    TextField,
)
import os

path = os.path.join(os.path.dirname(__file__), "数据库.sqlite")
db = SqliteDatabase(path)


class JJs(Model):
    "基金数据模型"
    code = CharField(column_name="基金代码")
    name = CharField(column_name="基金名字")
    money = FloatField(column_name="总市值")
    net = FloatField(column_name="净值")  # 净值
    url = TextField(column_name="网址")
    stocks = TextField(column_name="十大持仓")
    date = DateField(formats="%Y-%m-%d", column_name="更新日期")

    class Meta:
        database = db  # 链接数据库


class Stocks(Model):
    "股票数据模型"
    name = CharField(column_name="股票名称")
    n = IntegerField(column_name="持有基金个数")
    money = FloatField(column_name="持有基金市值")
    date = DateField(formats="%Y-%m-%d", column_name="更新日期")

    class Meta:
        database = db


if __name__ == "__main__":
    db.connect()
    db.create_tables([JJs, Stocks])
    db.close()
