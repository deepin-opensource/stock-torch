import matplotlib
from matplotlib import pyplot as plt
from tqdm import tqdm

matplotlib.use("TkAgg")  # 大小写无所谓 tkaGg ,TkAgg 都行
plt.rcParams["font.family"] = ["sans-serif"]  # 用来正常显示中文标签
plt.rcParams["font.sans-serif"] = ["SimHei"]  # 用来正常显示中文标签
plt.rcParams["axes.unicode_minus"] = False  # 用来正常显示负号

HEADER = "\033[1;95m"
OKBLUE = "\033[1;94m"
OKGREEN = "\033[1;92m"
OKRED = "\033[1;31m"
WARNING = "\033[1;93m"
FAIL = "\033[1;91m"
END = "\033[0m"
ENDL = "\033[0m"
ENDC = "\033[0m"
BOLD = "\033[1m"
UNDERLINE = "\033[1;4m"

